const express = require('express');
const app = express();

const MongoClient = require('mongodb').MongoClient;

const cors = require('cors');

// Connection URL
const url = 'mongodb://ec2-54-154-92-190.eu-west-1.compute.amazonaws.com:27017/amazon_products';
// Use connect method to connect to the server

MongoClient.connect(url, function(err, db) {
    if (err) {
        console.log('Unable to connect to the mongoDB server. Error:', err);
    } else {
        //HURRAY!! We are connected. :)
        console.log('Connection established to', url);
    }

    //Server starts listening
    app.listen(9999, function () {
        console.log('api-products server listening!');
    });

    // Find all products
    app.get('/products/:limit', cors(), function (req, res) {
        res.setHeader('Content-Type', 'application/json');

        var collection = db.collection('products_collection');

        var limit = Number(req.params.limit);

        collection.find({}).limit(limit).toArray(function(err, docs) {
            res.send(
                JSON.stringify(docs)
            );
        });
    });

    // Find one product
    app.get('/product/:productId', cors(), function (req, res) {
        res.setHeader('Content-Type', 'application/json');

        var collection = db.collection('products_collection');

        collection.find({_id: req.params.productId}).toArray(function(err, docs) {
            res.send(
                JSON.stringify(docs)
            );
        });
    });
});

