const express = require('express');
const app = express();

const MongoClient = require('mongodb').MongoClient;

const cors = require('cors');

var bodyParser = require('body-parser');

app.use(bodyParser.urlencoded({extended: false}));
app.use(bodyParser.json());

app.use(function(req, res, next) {
  res.header("Access-Control-Allow-Origin", "*");
  res.header("Access-Control-Allow-Methods", "GET,HEAD,OPTIONS,POST,PUT");
  res.header("Access-Control-Allow-Headers", "Origin, X-Requested-With, Content-Type, Accept, Authorization");
  next();
});

// Connection URL
const url = 'mongodb://52.211.154.226:27017/analytics';
// Use connect method to connect to the server

MongoClient.connect(url, function(err, db) {
    if (err) {
        console.log('Unable to connect to the mongoDB server. Error:', err);
    } else {
        //HURRAY!! We are connected. :)
        console.log('Connection established to', url);
    }

    //Server starts listening
    app.listen(9997, function () {
        console.log('api-ratesandsales server listening!');
    });

    app.post('/sales/save_sale/', cors(), function(req, res){
        var doc = JSON.parse(req.body);

        var collection = db.collection('sales_collection');

        collection.insert(doc);
    });

    app.post('/ratings/save_rate/', cors(), function (req, res) {
	console.log(req.body);

        var doc = JSON.parse(Object.keys(req.body)[0]);

        var collection = db.collection('ratings_collection');

        collection.insert(doc);
    });
});
