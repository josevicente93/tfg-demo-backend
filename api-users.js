const express = require('express');
const app = express();

const MongoClient = require('mongodb').MongoClient;

const ObjectID = require('mongodb').ObjectID;

const cors = require('cors');

// Connection URL
const url = 'mongodb://localhost:27017/tfg_users';
// Use connect method to connect to the server

MongoClient.connect(url, function(err, db) {
    if (err) {
        console.log('Unable to connect to the mongoDB server. Error:', err);
    } else {
        //HURRAY!! We are connected. :)
        console.log('Connection established to', url);
    }

    //Server starts listening
    app.listen(9998, function () {
        console.log('api-users server listening!');
    });

    // Find all users
    app.get('/users/:limit', cors(), function (req, res) {
        res.setHeader('Content-Type', 'application/json');

        var collection = db.collection('users_collection');

        var limit = Number(req.params.limit);

        collection.find({}).limit(limit).toArray(function(err, docs) {
            res.send(
                JSON.stringify(docs)
            );
        });
    });

    // Find one user
    /*app.get('/user/:userId', cors(), function (req, res) {
        res.setHeader('Content-Type', 'application/json');

        var collection = db.collection('users_collection');

        collection.find({_id: ObjectID(req.params.userId)}).toArray(function(err, docs) {
            res.send(
                JSON.stringify(docs)
            );
        });
    });*/

    // Find user id by name
    app.get('/user/:userName', cors(), function (req, res) {
        res.setHeader('Content-Type', 'application/json');

        var collection = db.collection('users_collection');

        collection.find({'login.username': req.params.userName}).toArray(function(err, docs) {
            res.send(
                JSON.stringify(docs)
            );
        });
    });
});

