#!/bin/bash

node api-products.js & > ./api-products-output.log

echo api-products PID: $! > api-products-pid.txt

node api-users.js & > ./api-users-output.log

echo user-products PID: $! > api-users-pid.txt

node api-ratesandsales.js & > ./api-ratesandsales-output.log

echo ratesandsales-products PID: $! > api-ratesandsales-pid.txt

